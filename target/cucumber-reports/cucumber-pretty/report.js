$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/Upload.feature");
formatter.feature({
  "name": "Search on google",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verify search on google",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@beforeEach"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User searchs query My query Main page",
  "keyword": "Given "
});
formatter.match({
  "location": "MainDefinitions.userSearchQuery(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verify presence of search result by value bla bla",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchDefinitions.checkPresenceOfSearchResult(String)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: [Element with search value is not present] \r\nExpecting:\r\n \u003cfalse\u003e\r\nto be equal to:\r\n \u003ctrue\u003e\r\nbut was not.\r\n\tat definitions.SearchDefinitions.checkPresenceOfSearchResult(SearchDefinitions.java:18)\r\n\tat ✽.User verify presence of search result by value bla bla(src/test/resources/features/Upload.feature:6)\r\n",
  "status": "failed"
});
});