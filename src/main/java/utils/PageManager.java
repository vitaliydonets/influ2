package utils;

import lombok.Getter;
import pages.MainPage;
import pages.SearchResultPage;
import pages.webdrivewrSite.DownloadPage;
import pages.webdrivewrSite.TopMenuPage;

public class PageManager {

    @Getter(lazy = true)
    private final PageManager pages = new PageManager();

    @Getter(lazy = true)
    private final MainPage mainPage = new MainPage();
    @Getter(lazy = true)
    private final SearchResultPage searchResultPage = new SearchResultPage();
    @Getter(lazy = true)
    private final TopMenuPage topMenuPage = new TopMenuPage();
    @Getter(lazy = true)
    private final DownloadPage downloadPage = new DownloadPage();

//    Lazy initialization
//    private MainPage mainPage;
//
//    public MainPage getMainPage() {
//        if (mainPage.equals(null) || mainPage == null) {
//            mainPage = new MainPage();
//        }
//        return mainPage;
//    }


}
