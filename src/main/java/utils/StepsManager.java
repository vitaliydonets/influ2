package utils;

import lombok.Getter;
import steps.DownloadSteps;
import steps.MainSteps;
import steps.SearchResultSteps;
import steps.TopMenuSteps;

public class StepsManager {

    PageManager pageManager = new PageManager();

    @Getter(lazy = true)
    private final MainSteps mainSteps = new MainSteps(pageManager.getPages());
    @Getter(lazy = true)
    private final SearchResultSteps searchResultSteps = new SearchResultSteps(pageManager.getPages());
    @Getter(lazy = true)
    private final TopMenuSteps topMenuStep = new TopMenuSteps(pageManager.getPages());
    @Getter(lazy = true)
    private final DownloadSteps downloadSteps = new DownloadSteps(pageManager.getPages());


}
