package utils;

import com.codeborne.selenide.Configuration;

public class DriverManager {

    public static void setUpDriver(){
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.holdBrowserOpen = true;
    }

}
