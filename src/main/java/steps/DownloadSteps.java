package steps;

import lombok.AllArgsConstructor;
import utils.PageManager;

@AllArgsConstructor
public class DownloadSteps {

    PageManager pages;

    public boolean is_present_title_download_page()
    { return pages.getDownloadPage().getTitle_page().isDisplayed();}


    public boolean is_present_selenium_downloads_tab_in_sidebar(){
        return pages.getDownloadPage().getSidebar_selenium_downloads().isDisplayed();
    }

    public boolean is_present_previous_releases_tab_in_sidebar(){
        return pages.getDownloadPage().getSidebar_previous_releases().isDisplayed();
    }

}
