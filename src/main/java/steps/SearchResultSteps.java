package steps;

import lombok.AllArgsConstructor;
import utils.PageManager;

@AllArgsConstructor
public class SearchResultSteps {

    private PageManager pages;

    public boolean isSearchElementPresent(String value){
        return pages.getSearchResultPage().getElementWithValue(value).isDisplayed();
    }

    public SearchResultSteps clickOnElementByValue(String value){
        pages.getSearchResultPage().getElementWithValue(value).click();
        return this;
    }




}
