package steps;

import lombok.AllArgsConstructor;
import utils.PageManager;

@AllArgsConstructor
public class MainSteps {

    private PageManager pages;

    public void enterToSearchFieldandPresEnter(String query) {
        pages.getMainPage().getSearchField().val(query).pressEnter();
    }

}
