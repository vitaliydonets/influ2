package steps;

import lombok.AllArgsConstructor;
import utils.PageManager;

@AllArgsConstructor
public class TopMenuSteps {

    PageManager pages;

//    public TopMenuSteps(PageManager pages){
//        this.pages = pages;
//    }

    public void clickOnDownload(){
        pages.getTopMenuPage().getDownloadTab().click();
    }

    public boolean isProjectTabPresent() {
        return pages.getTopMenuPage().getProjectsTab().isDisplayed();
    }

    public boolean isDownloadTabPresent() {
        return pages.getTopMenuPage().getDownloadTab().isDisplayed();
    }

}
