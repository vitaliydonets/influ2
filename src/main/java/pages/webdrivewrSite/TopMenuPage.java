package pages.webdrivewrSite;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class TopMenuPage {

    private SelenideElement projectsTab = $("#menu_projects");
    private SelenideElement downloadTab = $("#menu_download");
    private SelenideElement documentation = $("#menu_documentation");
}
