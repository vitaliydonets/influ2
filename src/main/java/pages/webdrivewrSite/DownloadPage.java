package pages.webdrivewrSite;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import static com.codeborne.selenide.Selenide.$x;

@Getter
public class DownloadPage {

    private SelenideElement title_page = $x("//h2[contains(text(),'Downloads')]");

    private SelenideElement sidebar_selenium_downloads = $x("//*[contains (text(), 'Selenium Downloads')]");
    private SelenideElement sidebar_previous_releases = $x("//*[contains (text(), 'Previous Releases')]");

}
