package pages;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class MainPage {

    private SelenideElement searchField = $("input#lst-ib");

}
