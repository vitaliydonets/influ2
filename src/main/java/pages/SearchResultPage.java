package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;

public class SearchResultPage {

    public SelenideElement getElementWithValue(String query) {
        return $x(format("//h3//*[contains(text(),'%s')]", query));

    }
}
