package definitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.AllArgsConstructor;
import utils.StepsManager;

import static java.lang.String.format;
import static org.assertj.core.api.Java6Assertions.assertThat;

@AllArgsConstructor
public class SearchResultDefinitions {

    StepsManager steps;

    @When("User click on first result with value (.*) on Search page")
    public void userclickOnFisrtsSearchResultLinkWithValue(String value){
        steps.getSearchResultSteps().clickOnElementByValue(value);
    }

    @Then("User verify presence of search result by value (.*) on Search page")
    public void checkPresenceOfSearchResult(String value){
        assertThat(steps.getSearchResultSteps().isSearchElementPresent(value))
                .as(format("Element with search value: '%s' is not present", value) )
                .isTrue();
    }

}
