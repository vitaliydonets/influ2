package definitions;

import cucumber.api.java.en.Then;
import lombok.AllArgsConstructor;
import utils.StepsManager;

import static org.assertj.core.api.Java6Assertions.assertThat;

@AllArgsConstructor
public class DownloadDifinitions {

    StepsManager steps;

    @Then("User verify presence of 'Selenium Downloads' in sidebar on Download page")
    public void check_presence_of_Selenium_Downloads_at_sidebar() {
        assertThat(steps.getDownloadSteps().is_present_selenium_downloads_tab_in_sidebar())
                .as("Element with search value: 'Selenium Downloads' tab is not present")
                .isTrue();
    }

    @Then("User verify presence of 'Previous Releases' in sidebar on Download page")
    public void check_presence_of_Previous_Releases_at_sidebar(){
        assertThat(steps.getDownloadSteps().is_present_previous_releases_tab_in_sidebar())
                .as("Element with search value: 'Selenium Downloads' tab is not present")
                .isTrue();
        }

    @Then("User went to the Download page and checked the 'Title' of this page")
    public void check_the_title(){
        assertThat(steps.getDownloadSteps().is_present_title_download_page())
                .as("The 'Title' is not present on this page")
                .isTrue();
    }

}
