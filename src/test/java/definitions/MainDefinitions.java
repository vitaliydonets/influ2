package definitions;

import cucumber.api.java.en.Given;
import lombok.AllArgsConstructor;
import utils.StepsManager;

@AllArgsConstructor
public class MainDefinitions {

    StepsManager step;

    @Given("User searchs query (.*) on Main page")
    public void userSearchQuery(String query){
        step.getMainSteps().enterToSearchFieldandPresEnter(query);
    }
}
