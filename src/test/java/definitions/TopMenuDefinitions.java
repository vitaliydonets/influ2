package definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import lombok.AllArgsConstructor;
import utils.StepsManager;

import static org.assertj.core.api.Java6Assertions.assertThat;

@AllArgsConstructor
public class TopMenuDefinitions {

    StepsManager steps;

    @Given("User click on 'Download Tab' from Topmenu page")
    public void clickTab(){
        steps.getTopMenuStep().clickOnDownload();
    }

    @Then("User verify presence of 'Project' tab at TopMenu page")
    public void checkPresenceOfProjectTab(){
        assertThat(steps.getTopMenuStep().isProjectTabPresent())
                .as("Element with search value: 'Project' tab is not present")
                .isTrue();
    }

    @Then("User verify presence of 'Download' tab at TopMenu page")
    public void checkPresenceOfDownloadTab(){
        assertThat(steps.getTopMenuStep().isDownloadTabPresent())
                .as("Element with search value: 'Download' tab is not present")
                .isTrue();
    }



}
