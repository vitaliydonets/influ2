package before;

import cucumber.api.java.Before;

import static com.codeborne.selenide.Selenide.open;
import static utils.DriverManager.setUpDriver;

public class BeforeMainSelenium {

    @Before("@beforeSelenium")
    public void beforeEach(){
        setUpDriver();
        open("https://www.seleniumhq.org/");
    }
}
