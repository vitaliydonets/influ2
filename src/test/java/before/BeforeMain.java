package before;

import cucumber.api.java.Before;

import static com.codeborne.selenide.Selenide.open;
import static utils.DriverManager.setUpDriver;

public class BeforeMain {

    @Before("@beforeEach")
    public void beforeEach(){
        setUpDriver();
        open("http://google.com");
    }

}
